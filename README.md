
## Weather Test APP
### En esta aplicación de test se han utilizado las siguientes librerías:
- "http": Para la interactuar con el servicio API de https://openweathermap.org/
- "equatable": Comparacion de objetos.
- "bloc": Patron utilizado para separar UI con la logica de negocio (https://bloclibrary.dev/)
- "get": Simplicidad para organizar las rutas y mas.
- "intl": Internacionalizacion de la app.
- "geolocator": Para obtener la ubicacion actual del usuario.

### Secuencia y contenido de la app:
- Al abrirse la app se pedirá los permisos necesarios para poder obtener la ubicacion actual.
- Se mostrará el clima actual segun ubicacion (temperatura en celcius, mínima, máxima, última actualización) y para los proximos 5 días (temperatura mínima, máxima)
- En el appbar al usuario se le ofrecera un botón de buscar para poder seleccionar 5 ciudades (La Plata, Cupertino, París, Turín, Barcelona).
- Tambien tendrá la posibilidad de volver a consultar su clima en su posicion actual.

### Arquitectura y organización de carpetas:
- blocs: Archivos correspondientes al patron bloc (eventos, blocs y estados). Es donde se programa la logica de negocio.
- data: Modelos de clases, servicios y respositorios para comunicarse a la API de la manera mas prolija y optimizada.
- presentation: Archivos exclusivamente de UI (screens y widgets custom)
- utils: Carpeta donde se vuelcan los archivos que son de utilidad en lo largo del proyecto.

### Screenshots
<img src="./assets/screenshots/screen1.png" width="248" height="537"/>
<img src="./assets/screenshots/screen2.png" width="248" height="537"/>
<img src="./assets/screenshots/screen3.png" width="248" height="537"/>
