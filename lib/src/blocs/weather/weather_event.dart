import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:test_flutter/src/data/models/coordinates_model.dart';

abstract class WeatherEvent extends Equatable {
  const WeatherEvent();
}

class LoadWeatherRequested extends WeatherEvent {
  final Coordinates coordinates;

  const LoadWeatherRequested({@required this.coordinates}) : assert(coordinates != null);

  @override
  List<Object> get props => [coordinates];
}

class LoadWeatherWithGeolocationRequested extends WeatherEvent {

  const LoadWeatherWithGeolocationRequested();

  @override
  List<Object> get props => [];
}
