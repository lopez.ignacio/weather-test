import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:test_flutter/src/data/models/weather_model.dart';
import 'package:test_flutter/src/data/models/weatherdaily_model.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();
  
  @override
  List<Object> get props => [];
}

class LoadWeatherUnitialized extends WeatherState {}

class LoadWeatherInProgress extends WeatherState {}

class LoadWeatherSuccess extends WeatherState {
  final Weather currentWeather;
  final List<WeatherDaily> nextDaysWeather;

  const LoadWeatherSuccess({@required this.currentWeather, @required this.nextDaysWeather});

  @override
  List<Object> get props => [currentWeather, nextDaysWeather];
}

class LoadWeatherFailure extends WeatherState {
  final String message;

  const LoadWeatherFailure({@required this.message});

  @override
  List<Object> get props => [message];
}
