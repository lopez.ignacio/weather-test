import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:test_flutter/src/blocs/weather/blocs.dart';
import 'package:test_flutter/src/data/models/coordinates_model.dart';

import 'package:test_flutter/src/data/models/weather_model.dart';
import 'package:test_flutter/src/data/models/weatherdaily_model.dart';
import 'package:test_flutter/src/data/repositories/weather_repository.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository weatherRepository;

  WeatherBloc({@required this.weatherRepository})
    : assert(weatherRepository != null), super(LoadWeatherUnitialized());

  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async* {
    if (event is LoadWeatherRequested) {
      yield* _mapLoadWeatherRequestedToState(event);
    } else if (event is LoadWeatherWithGeolocationRequested) {
      yield * _mapLoadWeatherWithGeolocationRequestedToState();
    }
  }

  /* 
  * Se obtiene el clima segun coordenadas.
  */
  Stream<WeatherState> _mapLoadWeatherRequestedToState(LoadWeatherRequested event) async* {
    yield LoadWeatherInProgress();
    try {
      final Weather weather = await weatherRepository.getWeather(event.coordinates);
      final List<WeatherDaily> weatherList = await weatherRepository.getNextDaysWeather(event.coordinates);
      yield LoadWeatherSuccess(currentWeather: weather, nextDaysWeather: weatherList);
    } catch (_) {
      yield LoadWeatherFailure(message: 'Ocurrió un error al obtener el clima.');
    }
  }

  /* 
  * Se obtiene el clima segun la localizacion actual.
  */
  Stream<WeatherState> _mapLoadWeatherWithGeolocationRequestedToState() async* {
    yield LoadWeatherInProgress();
    try {
      final Coordinates coordinates = await weatherRepository.getCurrentLocation();
      yield* _mapLoadWeatherRequestedToState(LoadWeatherRequested(coordinates: coordinates));
    } catch (_) {
      yield LoadWeatherFailure(message: 'Ocurrió un error al obtener el clima con localización actual.');
    }
  }
}
