import 'package:flutter/material.dart';
import 'package:test_flutter/src/data/models/coordinates_model.dart';
import 'package:test_flutter/src/data/models/weather_model.dart';
import 'package:test_flutter/src/data/models/weatherdaily_model.dart';
import 'package:test_flutter/src/data/services/location_service.dart';
import 'package:test_flutter/src/data/services/weather_service.dart';

class WeatherRepository {
  final WeatherService weatherService;
  final Locationservice locationservice;

  WeatherRepository({@required this.weatherService, @required this.locationservice})
    : assert(weatherService != null);

  Future<Weather> getWeather(Coordinates coord) async {
    return weatherService.fetchWeather(coord);
  }

  Future<List<WeatherDaily>> getNextDaysWeather(Coordinates coord) async {
    return weatherService.fetchNextDaysWeather(coord);
  }

  Future<Coordinates> getCurrentLocation() async {
    return locationservice.fetchCurrentLocation();
  }
}
