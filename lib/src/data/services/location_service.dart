
import 'package:geolocator/geolocator.dart';
import 'package:test_flutter/src/data/models/coordinates_model.dart';

class Locationservice {
  Locationservice();

  Future<Coordinates> fetchCurrentLocation() async {
    try {
      Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      String latitude = position.latitude.toString();
      String longitude = position.longitude.toString();
    
      Coordinates coordinates = new Coordinates(lat: latitude, lon:longitude);
      return coordinates;
    } catch (e) {
      throw Exception(e);
    }
  }
}
