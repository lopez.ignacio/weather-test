import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:test_flutter/src/data/models/coordinates_model.dart';
import 'package:test_flutter/src/data/models/weather_model.dart';
import 'package:test_flutter/src/data/models/weatherdaily_model.dart';
import 'package:test_flutter/src/utils/constants.dart';

class WeatherService {
  final http.Client httpClient;

  WeatherService({@required this.httpClient}) : assert(httpClient != null);

  Future<Weather> fetchWeather(Coordinates coordinates) async {
    final url = '$BASE_URL/weather?lat=${coordinates.lat}&lon=${coordinates.lon}&$KEY';
    final response = await this.httpClient.get(url);

    if (response.statusCode != 200) {
      throw Exception('Ocurrio un error al obtener el clima.');
    }

    return Weather.fromJson(response.body);
  }

  Future<List<WeatherDaily>> fetchNextDaysWeather(Coordinates coordinates) async {
    final url = '$BASE_URL/onecall?lat=${coordinates.lat}&lon=${coordinates.lon}&$KEY';
    final dynamic response = await this.httpClient.get(url);

    if (response.statusCode != 200) {
      throw Exception('Ocurrio un error al obtener los proximos días del clima.');
    }

    final weatherList = WeatherDailyList.fromJsonList(json.decode(response.body)['daily']);
    return weatherList.items.sublist(1,6);
  }
}
