import 'dart:convert';
import 'package:equatable/equatable.dart';
import 'package:test_flutter/src/data/models/coordinates_model.dart';
import 'package:test_flutter/src/utils/cities.dart';

class City extends Equatable {
  final String name;
  final Coordinates coordinates;
  final String country;

  City({
    this.name,
    this.coordinates,
    this.country
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'coordinates': coordinates?.toMap(),
      'country': country,
    };
  }

  factory City.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return City(
      name: map['name'],
      coordinates: Coordinates.fromMap(map['coordinates']),
      country: map['country_cod'],
    );
  }

  String toJson() => json.encode(toMap());

  static City fromJson(String source) => City.fromMap(json.decode(source));

  @override
  List<Object> get props => [name, coordinates, country];

  @override
  String toString() {
    return 'City(name: $name, coordinates: $coordinates, country: $country)';
  }
}

class CityList {
  List<City> items = new List();
  CityList();

  CityList.fromList() {
    var jsonList = cities['list'];
    if (jsonList == null) return;
  
    for(var item in jsonList) {
      final weather = new City.fromMap(item);
      items.add(weather);
    }
  }
}
