
import 'dart:convert';

import 'package:equatable/equatable.dart';

class Weather extends Equatable {
  final int id;
  final int cod;
  final int timezone;
  final String name;
  final WeatherCondition weatherCondition;
  final MainWeather mainWeather;
  final int lastUpdate;

  Weather({
    this.id,
    this.cod,
    this.timezone,
    this.name,
    this.weatherCondition,
    this.mainWeather,
    this.lastUpdate
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'cod': cod,
      'timezone': timezone,
      'name': name,
      'weather': weatherCondition?.toMap(),
      'main': mainWeather,
      'dt': lastUpdate
    };
  }

  factory Weather.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return Weather(
      id: map['id'],
      cod: map['cod'],
      timezone: map['timezone'],
      name: map['name'],
      weatherCondition: WeatherCondition.fromMap(map['weather'][0]),
      mainWeather: MainWeather.fromMap(map['main']),
      lastUpdate: map['dt'],
    );

  }

  String toJson() => json.encode(toMap());

  static Weather fromJson(String source) => Weather.fromMap(json.decode(source));
  
  @override
  List<Object> get props => [id, cod, timezone, name, weatherCondition, lastUpdate];

  @override
  String toString() {
    return 'Weather(id: $id, cod: $cod, timezone: $timezone, name: $name, weatherCondition: $weatherCondition)';
  }
}

class WeatherCondition extends Equatable {
  final String main;
  final String description;
  final String icon;

  WeatherCondition({
    this.main,
    this.description,
    this.icon
  });

  Map<String, dynamic> toMap() {
    return {
      'main': main,
      'description': description,
      'icon': icon,
    };
  }

  factory WeatherCondition.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return WeatherCondition(
      main: map['main'],
      description: map['description'],
      icon: map['icon'],
    );
  }

  String toJson() => json.encode(toMap());

  static WeatherCondition fromJson(String source) => WeatherCondition.fromMap(json.decode(source));

  @override
  List<Object> get props => [main, description];

  @override
  String toString() => 'WeatherCondition(main: $main, description: $description, icon: $icon)';
}

class MainWeather extends Equatable {
  final int temp;
  final int feelsLike;
  final int tempMin;
  final int tempMax;
  final int pressure;
  final int humidity;

  MainWeather({
    this.temp,
    this.feelsLike,
    this.tempMin,
    this.tempMax,
    this.pressure,
    this.humidity
  });

  Map<String, dynamic> toMap() {
    return {
      'temp': temp,
      'feels_like': feelsLike,
      'temp_min': tempMin,
      'temp_max': tempMax,
      'pressure': pressure,
      'humidity': humidity,
    };
  }

  factory MainWeather.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return MainWeather(
      temp: map['temp'].round(),
      feelsLike: map['feels_like'].round(),
      tempMin: map['temp_min'].round(),
      tempMax: map['temp_max'].round(),
      pressure: map['pressure'].round(),
      humidity: map['humidity'].round(),
    );
  }

  String toJson() => json.encode(toMap());

  static MainWeather fromJson(String source) => MainWeather.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Main(temp: $temp, feels_like: $feelsLike, temp_min: $tempMin, temp_max: $tempMax, pressure: $pressure, humidity: $humidity)';
  }

  @override
  List<Object> get props => [temp, feelsLike, tempMin, tempMax, pressure, humidity];
}

class WeatherList {
  List<Weather> items = new List();
  WeatherList();

  WeatherList.fromJsonList(List<dynamic> jsonList) {
    if(jsonList ==  null) return;

    for(var item in jsonList) {
      final weather = new Weather.fromMap(item);
      items.add(weather);
    }
  }
}
