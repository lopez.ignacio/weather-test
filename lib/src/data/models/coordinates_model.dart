import 'dart:convert';

import 'package:equatable/equatable.dart';

class Coordinates extends Equatable {
  final String lon;
  final String lat;

  Coordinates({this.lat, this.lon});

  Map<String, dynamic> toMap() {
    return {
      'lon': lon,
      'lat': lat,
    };
  }

  factory Coordinates.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return Coordinates(
      lon: map['lon'],
      lat: map['lat'],
    );
  }

  String toJson() => json.encode(toMap());

  @override
  List<Object> get props => [lon, lat];

  static Coordinates fromJson(String source) => Coordinates.fromMap(json.decode(source));
}
