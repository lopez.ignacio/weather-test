import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:test_flutter/src/data/models/weather_model.dart';

class WeatherDaily extends Equatable {
  final int dt;
  final Temperature temp;
  final WeatherCondition weatherCondition;

  WeatherDaily({this.dt, this.temp, this.weatherCondition});
  
  Map<String, dynamic> toMap() {
    return {
      'dt': dt,
      'temp': temp?.toMap(),
      'weather': weatherCondition?.toMap(),
    };
  }

  factory WeatherDaily.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return WeatherDaily(
      dt: map['dt'],
      temp: Temperature.fromMap(map['temp']),
      weatherCondition: WeatherCondition.fromMap(map['weather'][0]),
    );
  }

  String toJson() => json.encode(toMap());

  static WeatherDaily fromJson(String source) => WeatherDaily.fromMap(json.decode(source));

  @override
  List<Object> get props => [dt, temp, weatherCondition];

  @override
  String toString() => 'WeatherDaily(dt: $dt, temp: $temp, weatherCondition: $weatherCondition)';
}

class Temperature extends Equatable {
  final int day;
  final int min;
  final int max;
  final int night;
  final int eve;
  final int morn;

  Temperature({
    this.day,
    this.min,
    this.max,
    this.night,
    this.eve,
    this.morn
  });

  Map<String, dynamic> toMap() {
    return {
      'day': day,
      'min': min,
      'max': max,
      'night': night,
      'eve': eve,
      'morn': morn,
    };
  }

  factory Temperature.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return Temperature(
      day: map['day'].round(),
      min: map['min'].round(),
      max: map['max'].round(),
      night: map['night'].round(),
      eve: map['eve'].round(),
      morn: map['morn'].round(),
    );
  }

  String toJson() => json.encode(toMap());

  static Temperature fromJson(String source) => Temperature.fromMap(json.decode(source));

  @override
  List<Object> get props => [day, min, max, night, eve, morn];

  @override
  String toString() {
    return 'Temperature(day: $day, min: $min, max: $max, night: $night, eve: $eve, morn: $morn)';
  }
}

class WeatherDailyList {
  List<WeatherDaily> items = new List();
  WeatherDailyList();

  WeatherDailyList.fromJsonList(List<dynamic> jsonList) {
    if(jsonList ==  null) return;

    for(var item in jsonList) {
      final weatherDaily = new WeatherDaily.fromMap(item);
      items.add(weatherDaily);
    }
  }
}
