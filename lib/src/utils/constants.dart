// Screens
const SCREEN_HOME           = '/home';
const SCREEN_CITYSELECTION  = '/cityselection';

// Utils services
const BASE_URL = 'https://api.openweathermap.org/data/2.5';
const KEY = 'appid=dff6bdb7504ec55f12062b35d2b0ef2e&lang=es&units=metric';