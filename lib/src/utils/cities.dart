Map cities = {
  "list": [
    {
      "name": "La Plata",
      "coordinates": {
        "lat": "-34.921452",
        "lon": "-57.954529"
      },
      "country_cod": "AR"
    },
    {
      "name": "Cupertino",
      "coordinates": {
        "lat": "37.323002",
        "lon": "-122.032181"
      },
      "country_cod": "US"
    },
    {
      "name": "Turín",
      "coordinates": {
        "lat": "45.070492",
        "lon": "7.68682"
      },
      "country_cod": "IT"
    },
    {
      "name": "Barcelona",
      "coordinates": {
        "lat": "41.38879",
        "lon": "2.15899"
      },
      "country_cod": "ES"
    },
    {
      "name": "París",
      "coordinates": {
        "lat": "48.853401",
        "lon": "2.3486"
      },
      "country_cod": "FR"
    }
  ]
};
