import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test_flutter/src/data/models/weather_model.dart';
import 'package:test_flutter/src/presentation/widgets/icon_weather.dart';

class CurrentWeather extends StatelessWidget {
  final Weather currentWeather;

  CurrentWeather({@required this.currentWeather})
    : assert(currentWeather != null);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _location(),
        _lastUpdated(context),
        _combinedWeather(),
      ],
    );
  }

  Widget _location() {
    return Padding(
      padding: const EdgeInsets.only(top: 0.0),
      child: Text(
        currentWeather.name,
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _lastUpdated(BuildContext context) {
    var date = DateTime.fromMillisecondsSinceEpoch(currentWeather.lastUpdate * 1000);
    var formattedDate = DateFormat.Hm().format(date);
    return Text(
      'Ultima actualización: $formattedDate',
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w200,
        color: Colors.white,
      ),
    );
  }

  Widget _combinedWeather() {
    return Padding(
      padding: EdgeInsets.only(top: 20.0, bottom: 50.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _temperature(),
          Text(
            currentWeather.weatherCondition.description,
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w200,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget _temperature() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconWeather(icon: currentWeather.weatherCondition.icon),
        Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: Text(
            '${currentWeather.mainWeather.temp}° C',
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.w600,
              color: Colors.white,
            ),
          ),
        ),
        Column(
          children: [
            Text(
              'max: ${currentWeather.mainWeather.tempMax}°',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w100,
                color: Colors.white,
              ),
            ),
            Text(
              'min: ${currentWeather.mainWeather.tempMin}°',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w100,
                color: Colors.white,
              ),
            )
          ],
        )
      ],
    );
  }
}
