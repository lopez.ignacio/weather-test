import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test_flutter/src/data/models/weatherdaily_model.dart';
import 'package:test_flutter/src/presentation/widgets/icon_weather.dart';

class NextDaysWeather extends StatelessWidget {
  final List<WeatherDaily> nextDaysWeather;

  NextDaysWeather({@required this.nextDaysWeather})
    : assert(nextDaysWeather != null);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return _listWeather(screenSize);
  }

  Widget _listWeather(dynamic screenSize) {
    return Container(
      height: screenSize.height * 0.3,
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: nextDaysWeather.length,
        itemBuilder: (context, index) {
          return _dayWeather(nextDaysWeather[index]);
        },
      ),
    );
  }

  Widget _dayWeather(WeatherDaily weatherDaily) {
    var date = DateTime.fromMillisecondsSinceEpoch(weatherDaily.dt * 1000);
    var formattedDate = DateFormat.EEEE('es').format(date);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50),
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: IconWeather(icon: weatherDaily.weatherCondition.icon, width: 30),
          ),
          Expanded(
            child: Text(
              '$formattedDate', 
              style: TextStyle(color: Colors.white, fontSize: 16.0)
            )
          ),
          Row(
            children: <Widget>[
              Text(
                '${weatherDaily.temp.max}°  ', 
                style: TextStyle(fontSize: 16.0, color: Colors.white)
              ),
              Text('${weatherDaily.temp.min}°', 
                style: TextStyle(fontSize: 16.0, color: Colors.grey[300])
              ),
            ],
          ),
        ],
      ),
    );
  }
}
