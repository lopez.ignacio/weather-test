import 'package:flutter/material.dart';

class IconWeather extends StatelessWidget {
  final String icon;
  final double width;

  IconWeather({@required this.icon, this.width});

  @override
  Widget build(BuildContext context) {
    if (icon != null) {
      return Container(
        width: width != null ? width : 100.0,
        child: FadeInImage.assetNetwork(
            placeholder: 'assets/cloud-loading.gif',
            image:'http://openweathermap.org/img/wn/$icon@2x.png'
        )
      );
    }
    return Container();
  }
}
