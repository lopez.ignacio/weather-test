import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:test_flutter/src/blocs/weather/blocs.dart';
import 'package:test_flutter/src/data/models/coordinates_model.dart';
import 'package:test_flutter/src/presentation/widgets/current_weather.dart';
import 'package:test_flutter/src/presentation/widgets/loading.dart';
import 'package:test_flutter/src/presentation/widgets/weather_daily.dart';
import 'package:test_flutter/src/utils/constants.dart';

class WeatherScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 0.0,
        leading: _locationIcon(context),
        title: Text('Weather Test'),
        actions: <Widget>[
          _searchIcon(context),
        ],
      ),
      body: Container(
        decoration: _backgroundGradient(),
        child: _builderWeather()
      )
    );
  }

  Widget _builderWeather() {
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (context, state) {
        if (state is LoadWeatherInProgress) {
          return Loading();
        }
        if (state is LoadWeatherFailure) {
          return Center(
            child: Text(state.message, 
              style: TextStyle(color: Colors.white)
            )
          );
        }
        if (state is LoadWeatherSuccess) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CurrentWeather(currentWeather: state.currentWeather),
              NextDaysWeather(nextDaysWeather: state.nextDaysWeather)
            ],
          );
        }
        return Container();
      },
    );
  }

  Widget _locationIcon(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.gps_fixed),
      onPressed: () async {
        _requestWeatherWithGeolocation(context);
      },
    );
  }

  Widget _searchIcon(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.search),
      onPressed: () async {
        var city = await Get.toNamed(SCREEN_CITYSELECTION);
        if (city!= null) {
          _requestWeather(context, city.coordinates);
        }
      },
    );
  }

  void _requestWeather(BuildContext context, Coordinates coordinates) {
    BlocProvider.of<WeatherBloc>(context)
      .add(LoadWeatherRequested(coordinates: coordinates));
  }

  void _requestWeatherWithGeolocation(BuildContext context) {
    BlocProvider.of<WeatherBloc>(context)
      .add(LoadWeatherWithGeolocationRequested());
  }

  BoxDecoration _backgroundGradient() {
    return BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topRight,
        colors: [
          Colors.deepPurple[400],
          Colors.blue[300],
        ],
      ),
    );
  }
}
