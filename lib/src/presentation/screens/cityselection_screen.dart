import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_flutter/src/data/models/city_model.dart';

class CitySelection extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var cities = CityList.fromList().items;
    return Scaffold(
      appBar: AppBar(
        title: Text('Ciudades'),
      ),
      body: ListView.separated(
        itemCount: cities.length,
        itemBuilder: (context, index) {
          var city = cities[index];
          return _itemCity(city);
        },
        separatorBuilder: (context, index) => Divider(
          color: Colors.grey,
        ),
      ),
    );
  }

  Widget _itemCity(City city) {
    return ListTile(
      title: Text('${city.name} (${city.country})'),
      onTap: () => Get.back(result: city),
    );
  }
}
