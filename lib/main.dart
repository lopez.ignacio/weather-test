import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:test_flutter/src/blocs/weather/blocs.dart';
import 'package:test_flutter/src/data/repositories/weather_repository.dart';
import 'package:test_flutter/src/data/services/location_service.dart';
import 'package:test_flutter/src/data/services/weather_service.dart';
import 'package:test_flutter/src/blocs/simple_bloc_observer.dart';
import 'package:http/http.dart' as http;
import 'package:test_flutter/src/presentation/screens/cityselection_screen.dart';
import 'package:test_flutter/src/presentation/screens/weather_screen.dart';
import 'package:test_flutter/src/utils/constants.dart';

void main() {
  Bloc.observer = SimpleBlocObsever();
  final WeatherRepository weatherRepository = WeatherRepository(
    weatherService: WeatherService(
      httpClient: http.Client(),
    ),
    locationservice: Locationservice()
  );

  runApp(TestFlutterApp(weatherRepository: weatherRepository));
}

class TestFlutterApp extends StatelessWidget {
  final WeatherRepository weatherRepository;

  const TestFlutterApp({
    @required this.weatherRepository, 
    Key key
  }) : assert(weatherRepository != null), super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: SCREEN_HOME,
      getPages: [
        GetPage(name: SCREEN_HOME, page: () => BlocProvider<WeatherBloc>(
          create: (BuildContext context) => WeatherBloc(
            weatherRepository: weatherRepository
          )..add(LoadWeatherWithGeolocationRequested()),
          child: WeatherScreen(),
        )),
        GetPage(name: SCREEN_CITYSELECTION, page: () => CitySelection())
      ],
      supportedLocales:  [
        const Locale('es', 'AR'),
      ],
      localizationsDelegates : [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      theme: ThemeData(primaryColor: Colors.deepPurple[400]),
    );
  }
}
